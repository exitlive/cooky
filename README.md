# Cooky

An [HTTP Cookies](https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies) implementation for the client.

## Usage

```dart
import 'package:cooky/cooky.dart' as cookie;

setCookie() async {
  cookie.set('key', 'value');
}
```

See our [example/example.dart](example/example.dart) for a more complete example. 