library cooky.tool.grind;

import 'dart:io' hide ProcessException;
import 'dart:mirrors';

import 'package:grinder/grinder.dart';

final Directory projectRoot =
    Directory.fromUri(currentMirrorSystem().findLibrary(#cooky.tool.grind).uri)
        .parent
        .parent
        .absolute;

@Task('Runs dartanalyzer and fails if there is a hint, warning or lint error')
void analyze() async {
  await runAsync('dartanalyzer',
      arguments: ['.', '--fatal-hints', '--fatal-warnings', '--fatal-lints']);
}

@Task()
void checkFormat() {
  if (DartFmt.dryRun(projectRoot)) {
    fail('Code is not properly formatted. Run `grind format`');
  }
}

@Task()
void format() => DartFmt.format(projectRoot);

@Task()
void testUnit() => TestRunner().testAsync(
    files: Directory('test'), platformSelector: ['chrome', 'firefox']);

@Task()
@Depends(checkFormat, analyze, testUnit)
void test() => true;

/// Setup grinder and logging.
void main(List<String> args) {
  grind(args);
}
