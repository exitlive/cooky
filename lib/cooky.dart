import 'dart:html';

/// Get the value of the cookie with name [key].
String? get(String key) {
  final cookies = document.cookie?.split('; ') ?? [];
  for (var cookie in cookies) {
    var parts = cookie.split('=');
    var name = Uri.decodeComponent(parts[0]);
    if (key == name) {
      return parts[1].isNotEmpty ? Uri.decodeComponent(parts[1]) : null;
    }
  }
  return null;
}

/// Set a cookie with name [key] and [value].
///
/// If the cookie already exists, it gets overwritten.
///
/// [maxAge] is added as a convenience, but always converted to [expires].
/// If both [maxAge] and [expires] are provided, then [maxAge] will overwrite
/// [expires] (this follows the cookie spec).
void set(
  String key,
  String value, {
  DateTime? expires,
  Duration? maxAge,
  String? path,
  String? domain,
  bool? secure,
}) {
  if (maxAge != null) expires = DateTime.now().add(maxAge);

  var cookie = ([
    Uri.encodeComponent(key),
    '=',
    Uri.encodeComponent(value),
    expires != null
        ? '; expires=' + formatDate(expires)
        : '', // use expires attribute, max-age is not supported by IE
    path != null ? '; path=' + path : '',
    domain != null ? '; domain=' + domain : '',
    secure != null && secure == true ? '; secure' : ''
  ].join(''));
  document.cookie = cookie;
}

/// Returns `true` if the key was found and the value removed.
/// Returns `false` if the key was not found.
bool remove(String key, {String? path, String? domain, bool? secure}) {
  if (get(key) != null) {
    set(
      key,
      '',
      expires: DateTime.fromMillisecondsSinceEpoch(0),
      path: path,
      domain: domain,
      secure: secure,
    );
    return true;
  }
  return false;
}

const _weekdays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
const _months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec'
];

String _pad(int number) => '$number'.padLeft(2, '0');

/// Manually sets the weekday and month because the 'en_US' locale might not be
/// initialized.
String formatDate(DateTime date) {
  date = date.toUtc();
  final weekday = _weekdays[date.weekday - 1];
  final month = _months[date.month - 1];
  return '$weekday, ${_pad(date.day)} $month ${date.year} ${_pad(date.hour)}:${_pad(date.minute)}:${_pad(date.second)} UTC';
}
