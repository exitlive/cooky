# Changelog

## 2.0.0

- Null safety support

## 1.1.0

- Update all dependencies and fix warnings
- Make the `formatDate` function public
- Remove the `intl` dependency
- Fix weekday `Thi` -> `Thu`

## 1.0.1

- Minor change: Use `DateTime.now().add(maxAge)` instead of building
  the date with `DateTime.fromMillisecondsSinceEpoch(/* etc... */)`

## 1.0.0

- Initial commit