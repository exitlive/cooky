import 'dart:html';

@TestOn('browser')
import 'package:test/test.dart';

import 'package:cooky/cooky.dart';

void main() {
  group('cookie', () {
    final unicode = r'ü()<>@,;:\"/[]?={}+-';
    final encoded = Uri.encodeComponent(unicode);

    group('set()', () {
      test('should be able to store a value', () async {
        set('test', 'min');
        expect(document.cookie?.contains('test=min'), isTrue);
      });

      test('should be able to set store a value with all arguments', () async {
        set('test', 'max',
            maxAge: Duration(days: 5), path: 'test', domain: 'localhost');
        expect(document.cookie?.contains('test=max'), isTrue);
      });

      test('should be able to store all keys and values', () async {
        set(unicode, unicode);
        expect(document.cookie?.contains('$encoded=$encoded'), isTrue);
      });
    });

    group('get()', () {
      test('should retrieve values', () async {
        set('test', 'bar');
        set(unicode, unicode);
        expect(get('test'), 'bar');
        expect(get(unicode), unicode);
      });
    });

    group('remove()', () {
      test('should remove the value from the cookie', () async {
        expect(get('removetest'), isNull);
        set('removetest', 'bar');
        expect(get('removetest'), 'bar');
        final response = remove('removetest');
        expect(response, isTrue);
        expect(get('removetest'), isNull);
      });
    });
  });
  group('formatDate()', () {
    test('properly formats all dates', () {
      DateTime date;
      date = DateTime.utc(2020, 2, 1, 13, 30, 14);
      expect(formatDate(date), 'Sat, 01 Feb 2020 13:30:14 UTC');

      date = DateTime.utc(1999, 12, 31, 1, 1, 1);
      expect(formatDate(date), 'Fri, 31 Dec 1999 01:01:01 UTC');

      date = DateTime.fromMillisecondsSinceEpoch(0);
      expect(formatDate(date), 'Thu, 01 Jan 1970 00:00:00 UTC');
    });
  });
}
